import { Component, OnInit } from '@angular/core';
import { TimelineService } from './timeline.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.scss']
})
export class TimelineComponent implements OnInit {
  public screenshots$: Observable<Array<any>>;

  constructor(
    private timelineService: TimelineService
  ) { }

  ngOnInit() {
    this.screenshots$ = this.timelineService.getScreenshots();
  }

  public onMouseOver(event): void {
    console.log(event);
  }

}

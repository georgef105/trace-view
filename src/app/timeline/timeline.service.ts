import { Injectable } from '@angular/core';
import { Subject, BehaviorSubject, Observable } from 'rxjs';

import { filter, map } from 'rxjs/operators';

export interface Trace {
  metadata: any;
  traceEvents: Array<any>;
}

@Injectable({
  providedIn: 'root'
})
export class TimelineService {
  private trace$ = new BehaviorSubject<Trace | null>(null);

  constructor() { }

  public pushTraceFile(trace: Trace): void {
    this.trace$.next(trace);
  }

  public getScreenshots(): Observable<Array<any>> {
    return this.trace$.pipe(
      filter(trace => !!trace),
      map(trace => {
        return trace.traceEvents.filter(event => event.name === 'Screenshot');
      })
    );
  }

}

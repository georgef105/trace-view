import { Component } from '@angular/core';
import { TimelineService } from './timeline/timeline.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private timelineService: TimelineService
  ) { }

  public fileChange(event): void {
    const { files } = event.target;

    if (files.length) {
      const reader = new FileReader();
      reader.onload = this.onFileUploaded(files[0]);

      reader.readAsText(files[0]);
    }
  }

  private onFileUploaded(file: File): any {
    return event => {
      const { result } = event.target;
      console.log('result', JSON.parse(result));

      this.timelineService.pushTraceFile(JSON.parse(result));
    };
  }
}
